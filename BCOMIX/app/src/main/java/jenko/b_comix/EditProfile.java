package jenko.b_comix;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class EditProfile extends AppCompatActivity {

    Toolbar toolbar;

    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;

    EditText editNickname;
    EditText editAge;
    Button saveBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        editNickname = findViewById(R.id.edit_text_nickname);
        editAge = findViewById(R.id.edit_text_age);

        toolbar = (Toolbar) findViewById(R.id.toolbar4);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        saveBtn = findViewById(R.id.save_btn);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDatabase.child("nicknames").child(mAuth.getUid()).setValue(editNickname.getText().toString());
                mDatabase.child("users").child(mAuth.getUid()).child("age").setValue(editAge.getText().toString());
                mDatabase.child("users").child(mAuth.getUid()).child("nickname").setValue(editNickname.getText().toString());
                Toast.makeText(EditProfile.this, "New Information has been saved", Toast.LENGTH_SHORT).show();
                editAge.setText("");
                editNickname.setText("");
            }
        });
    }
}
