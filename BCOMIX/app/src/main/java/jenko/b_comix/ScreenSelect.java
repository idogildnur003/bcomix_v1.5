package jenko.b_comix;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class ScreenSelect extends AppCompatActivity {
    private ImageView dostbackround;
    private ImageView graybackround;
    private ImageView roombackround;
    private ImageView seabackround;
    private ImageView checksea;
    private ImageView checkgray;
    private ImageView checkdots;
    private ImageView checkroom;
    private Button button;

    private String mBackGround;
    private FirebaseAuth mAuth;
    private DocumentReference mDocRef;
    private GlobalVariables global;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_select);
        dostbackround = (ImageView) findViewById(R.id.dotbackround);
        graybackround = (ImageView) findViewById(R.id.graybackround);
        roombackround = (ImageView) findViewById(R.id.roombackround);
        seabackround = (ImageView) findViewById(R.id.seabackround);
        checksea = (ImageView) findViewById(R.id.checksea);
        checkgray = (ImageView) findViewById(R.id.checkgray);
        checkdots = (ImageView) findViewById(R.id.checkdots);
        checkroom = (ImageView) findViewById(R.id.checkroom);
        button = (Button) findViewById(R.id.button);

        mAuth = FirebaseAuth.getInstance();


        dostbackround.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button.setEnabled(true);
                dostbackround.setAlpha((float) 1);
                graybackround.setAlpha((float) 0.8);
                roombackround.setAlpha((float) 0.8);
                seabackround.setAlpha((float) 0.8);
                checkdots.bringToFront();
                checkdots.setVisibility(View.VISIBLE);
                checkgray.setVisibility(View.INVISIBLE);
                checkroom.setVisibility(View.INVISIBLE);
                checksea.setVisibility(View.INVISIBLE);
                mBackGround = "dot_back";
            }
        });

        roombackround.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button.setEnabled(true);
                roombackround.setAlpha((float) 1);
                graybackround.setAlpha((float) 0.8);
                dostbackround.setAlpha((float) 0.8);
                seabackround.setAlpha((float) 0.8);
                checkroom.bringToFront();
                checkroom.setVisibility(View.VISIBLE);
                checkgray.setVisibility(View.INVISIBLE);
                checkdots.setVisibility(View.INVISIBLE);
                checksea.setVisibility(View.INVISIBLE);
                mBackGround = "room_back";


            }
        });

        graybackround.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button.setEnabled(true);
                graybackround.setAlpha((float) 1);
                roombackround.setAlpha((float) 0.8);
                dostbackround.setAlpha((float) 0.8);
                seabackround.setAlpha((float) 0.8);
                checkgray.bringToFront();
                checkgray.setVisibility(View.VISIBLE);
                checkroom.setVisibility(View.INVISIBLE);
                checkdots.setVisibility(View.INVISIBLE);
                checksea.setVisibility(View.INVISIBLE);
                mBackGround = "gray_back";

            }
        });

        seabackround.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button.setEnabled(true);
                seabackround.setAlpha((float) 1);
                graybackround.setAlpha((float) 0.8);
                roombackround.setAlpha((float) 0.8);
                dostbackround.setAlpha((float) 0.8);
                checksea.bringToFront();
                checksea.setVisibility(View.VISIBLE);
                checkgray.setVisibility(View.INVISIBLE);
                checkroom.setVisibility(View.INVISIBLE);
                checkdots.setVisibility(View.INVISIBLE);
                mBackGround = "sea_backs_quare";

            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                global = new GlobalVariables();
                global.setScreen(mBackGround);
                startActivity(new Intent(ScreenSelect.this, ChatStruct.class));
                finish();

            }
        });
    }

}