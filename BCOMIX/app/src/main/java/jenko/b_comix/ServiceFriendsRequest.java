package jenko.b_comix;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.HandlerThread;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;
import java.util.ServiceConfigurationError;

import static jenko.b_comix.ChannelsManager.CHANNEL_1_ID;

public class ServiceFriendsRequest extends Service {

    FirebaseAuth mAuth;
    DatabaseReference mRequestsDB;
    PendingIntent pendingIntent;

    private NotificationManagerCompat notificationManager;


    public ServiceFriendsRequest() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(final Intent intent, int flags, int startId){
        mRequestsDB.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                String friendID = (String) dataSnapshot.getValue();
                if(!friendID.equals("none")) {
                    final String [] parts = {"","",""};
                    parts[0] = friendID.split("&&")[0];//type
                    parts[1] = friendID.split("&&")[1];//id
                    parts[2] = friendID.split("&&")[2];//name
                    if(parts[0].equals("request")){//someone send you a friend request
                        Intent intent1 = new Intent(ServiceFriendsRequest.this,ActivityAcceptFriendRequest.class);
                        intent1.putExtra("type","chat");
                        intent1.putExtra("id",parts[1]);
                        intent1.putExtra("nickname",parts[2]);
                        intent1.putExtra("title","Wanna to be buddy of "+parts[2]+"?");
                        pendingIntent = PendingIntent.getActivity(ServiceFriendsRequest.this,0,intent1,PendingIntent.FLAG_ONE_SHOT);
                        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(ServiceFriendsRequest.this, NotificationChannel.DEFAULT_CHANNEL_ID)
                                .setSmallIcon(R.drawable.logo_small)
                                .setContentTitle("New Friend request")
                                .setContentText(parts[2] + " want to be your buddy!")
                                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                .setAutoCancel(true)
                                .setSound(defaultSoundUri)
                                .setContentIntent(pendingIntent);
                        NotificationManagerCompat notificationCompat = NotificationManagerCompat.from(ServiceFriendsRequest.this);

                        notificationManager = NotificationManagerCompat.from(ServiceFriendsRequest.this);
                        Notification notification = new NotificationCompat.Builder(ServiceFriendsRequest.this, CHANNEL_1_ID)
                                .setSmallIcon(R.drawable.logo_small)
                                .setContentTitle("New Friend request")
                                .setContentText(parts[2] + " want to be your buddy!")
                                .setPriority(NotificationCompat.PRIORITY_HIGH)
                                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                                .setContentIntent(pendingIntent)
                                .setAutoCancel(true)
                                .build();

                        notificationManager.notify(1, notification);


                        notificationCompat = NotificationManagerCompat.from(ServiceFriendsRequest.this);
                        notificationCompat.notify(0, mBuilder.build());
                        dataSnapshot.getRef().setValue(null);
                    }
                    else if(parts[0].equals("yes")){
                        DatabaseReference myDB = FirebaseDatabase.getInstance().getReference().child("users").child(mAuth.getUid()).child("friends");
                        myDB.push().setValue(parts[1]);
                        dataSnapshot.getRef().setValue(null);
                    }
                    else if(parts[0].equals("group_invite")){
                        String senderid = parts[1];
                        String bookKey = parts[2].split("$$")[0];
                        String senderName = parts[2].split("$$")[1];
                        String bookName = parts[2].split("$$")[2];
                        Intent intent1 = new Intent(ServiceFriendsRequest.this,ActivityAcceptFriendRequest.class);
                        intent1.putExtra("type","group");
                        intent1.putExtra("id",bookKey);
                        intent1.putExtra("nickname",senderName);
                        intent1.putExtra("title",senderName+"Send you request to"+bookName+".");
                    }

                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }


        });
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        mAuth = FirebaseAuth.getInstance();
        mRequestsDB = FirebaseDatabase.getInstance().getReference().child("friends_request").child(mAuth.getUid());
        mRequestsDB.setValue("none");
    }

    @Override
    public void onDestroy() {
        Intent restartService = new Intent("ReciverFriendsRequests");
        sendBroadcast(restartService);
    }

}
