package jenko.b_comix;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

import static jenko.b_comix.ChannelsManager.CHANNEL_1_ID;

public class ServiceMessagesListener extends Service {

    DatabaseReference mDatabase;
    FirebaseAuth mAuth;
    DatabaseReference recent_chatsSB;
    Queue<String> recent_chats_queue;
    GlobalVariables global;
    PendingIntent pendingIntent;
    DatabaseReference mNicknamesDB;
    Map<String,Object> mNicknames;

    private NotificationManagerCompat notificationManager;

    public ServiceMessagesListener() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        mAuth = FirebaseAuth.getInstance();
        mNicknames = new HashMap<>();
        global = new GlobalVariables();
        if(mAuth.getUid()!=null) {
            mDatabase = FirebaseDatabase.getInstance().getReference().child("public_queues").child(mAuth.getUid());
            mNicknamesDB = FirebaseDatabase.getInstance().getReference().child("nicknames");
            recent_chatsSB = FirebaseDatabase.getInstance().getReference().child("users").child(mAuth.getUid()).child("recents");
        }
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        mNicknamesDB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mNicknames = (Map<String, Object>) dataSnapshot.getValue();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        recent_chats_queue = new PriorityQueue<>();
        mDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                ChatMessage msg = dataSnapshot.getValue(ChatMessage.class);
                DatabaseReference dstBookDB = FirebaseDatabase.getInstance().getReference().child("users").child(mAuth.getUid()).child("books").child(msg.mSrcUserID);
                dstBookDB.push().setValue(msg);
                dataSnapshot.getRef().setValue(null);
                if(recent_chats_queue!=null && recent_chats_queue.contains(msg.mSrcUserID)) {
                    recent_chats_queue.remove(msg.mSrcUserID);
                }
                recent_chats_queue.add(msg.mSrcUserID);
                List<String> temp = new ArrayList<>();
                temp.addAll(recent_chats_queue);
                recent_chatsSB.setValue(temp);

                global.setdstUserID(msg.mSrcUserID);
                Intent intent1 = new Intent(ServiceMessagesListener.this,CarouselPreviewActivity.class);
                pendingIntent = PendingIntent.getActivity(ServiceMessagesListener.this,0,intent1,PendingIntent.FLAG_ONE_SHOT);
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(ServiceMessagesListener.this, NotificationChannel.DEFAULT_CHANNEL_ID)
                        .setSmallIcon(R.drawable.logo_small)
                        .setContentTitle("New message received!")
                        .setContentText(mNicknames.get(msg.mSrcUserID) + " said:"+msg.mText)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
                NotificationManagerCompat notificationCompat = NotificationManagerCompat.from(ServiceMessagesListener.this);
                NotificationManager mNotificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager = NotificationManagerCompat.from(ServiceMessagesListener.this);
                Notification notification = new NotificationCompat.Builder(ServiceMessagesListener.this, CHANNEL_1_ID)
                        .setSmallIcon(R.drawable.logo_small)
                        .setContentTitle("New message received!")
                        .setContentText(mNicknames.get(msg.mSrcUserID) + " said:"+msg.mText)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .build();

                notificationManager.notify(1, notification);

                notificationCompat = NotificationManagerCompat.from(ServiceMessagesListener.this);
                notificationCompat.notify(0, mBuilder.build());

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) { }
            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) { }
            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) { }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) { }
        });


        recent_chatsSB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    List<String> temp = (List<String>) dataSnapshot.getValue();
                    recent_chats_queue.clear();
                    recent_chats_queue.addAll(temp);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Intent restartService = new Intent("ReciverMessagesService");
        sendBroadcast(restartService);
    }
}
