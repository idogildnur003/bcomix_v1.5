package jenko.b_comix;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

public class UserSlideAdapter extends BaseAdapter {

    private ArrayList<Character> data;
    private AppCompatActivity activity;

    public UserSlideAdapter(AppCompatActivity context, ArrayList<Character> objects) {
        this.activity = context;
        this.data = objects;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Character getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_flow_view, null, false);

            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.CharacterImage.setImageResource(data.get(position).getImageSource());

        return convertView;
    }


    private static class ViewHolder {
        private ImageView CharacterImage;

        public ViewHolder(View v) {
            CharacterImage = (ImageView) v.findViewById(R.id.image);
        }
    }
}