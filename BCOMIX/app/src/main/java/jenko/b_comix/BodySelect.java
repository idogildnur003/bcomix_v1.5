package jenko.b_comix;

import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BodySelect extends AppCompatActivity implements BubbleFragment.OnSelectClickListener{

    Button nextBtn;
    Bundle b;
    FirebaseAuth mAuth;
    DocumentReference mDocRef;
    LinearLayout contactsView;
    List<Map<String,Object>> mAllFragments;
    private DatabaseReference mDatabase;
    String mSelectedName;
    String mSelectedEmail;
    GlobalVariables global;
    AddBuddyFragment tempFragment;

    private ProgressDialog loading;
    private Boolean loadingFlag;

    boolean side; //true -> left, false -> right

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_body_select);
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("users").child(mAuth.getUid());
        mDatabase.addValueEventListener(body_select_listener);
        this.side = false;//start from right
        contactsView = findViewById(R.id.contactLinearLayout);
        mAllFragments = new ArrayList<>();

        loadingFlag = false;
        loading = new ProgressDialog(this);

        loading.show();
        loading.setMessage("Uploading Contacts...");
        loading.setCanceledOnTouchOutside(false);

        nextBtn = findViewById(R.id.next_btn);
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BodySelect.this,CarouselPreviewActivity.class);
                intent.putExtra("type","chat");
                intent.putExtra("dst",global.getDstUserID());
                intent.putExtra("name","none");
                for(Map<String,Object> fragment : mAllFragments){
                    BubbleFragment tempFragment = (BubbleFragment)fragment.get("fragment");
                    tempFragment.onDestroy();
                }
                startActivity(intent);
                finish();
            }
        });


        ImageView addFriendImageView = findViewById(R.id.addFriendImageView);
        addFriendImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                tempFragment = new AddBuddyFragment();
                Intent searchIntent = new Intent(BodySelect.this,AddBuddyFragment.class);
                startActivity(searchIntent);
            }
        });

    }


    ValueEventListener body_select_listener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if(dataSnapshot.exists()) {
                User user = dataSnapshot.getValue(User.class);
                Map<String,Object> friends = user.friends;
                if(friends != null && !friends.isEmpty()) {
                    DatabaseReference nicknamesDB = FirebaseDatabase.getInstance().getReference().child("nicknames");
                    for(final Object friendID : friends.values()){
                        nicknamesDB.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(dataSnapshot.exists()){
                                    Map<String, Object> temp_map = (Map<String, Object>) dataSnapshot.getValue();
                                    String name = (String) temp_map.get(friendID);
                                    BubbleFragment friendFragment = BubbleFragment.newInstance(name, side, (String) friendID);
                                    Map<String, Object> fragmentData = new HashMap<>();
                                    fragmentData.put("fragment", friendFragment);
                                    fragmentData.put("name", name);
                                    mAllFragments.add(fragmentData);
                                    getSupportFragmentManager()
                                            .beginTransaction()
                                            .add(R.id.contactLinearLayout, friendFragment, "BuubleContact")
                                            .commitAllowingStateLoss();

                                    side = !side;
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                }
                else
                {
                    //TODO: no friends
                }
                loadingFlag = true;
            }
            if (loadingFlag)
                loading.dismiss();
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            //problema
        }
    };

    @Override
    public void onSelectClickListener(String nickname, String userID) {
        nextBtn.setEnabled(true);
        for(Map<String,Object> fragment:mAllFragments){
            if(nickname.equals((String) fragment.get("name"))){
                this.global = new GlobalVariables();
                global.setName((String)fragment.get("name"));
                global.setEmail((String)fragment.get("email"));
                global.setdstUserID(userID);
                mSelectedName = (String)fragment.get("name");
                mSelectedEmail = (String)fragment.get("email");
            }
            else{
                BubbleFragment tempFragment = (BubbleFragment)fragment.get("fragment");
                tempFragment.hidecheck();
                FragmentManager fm = getFragmentManager();
                contactsView.childDrawableStateChanged(tempFragment.getView());
            }
        }
    }

}