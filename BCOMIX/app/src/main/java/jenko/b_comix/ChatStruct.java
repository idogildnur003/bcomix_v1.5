package jenko.b_comix;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow;

public class ChatStruct extends AppCompatActivity {

    FirebaseAuth mAuth;
    DatabaseReference mDatabase;
    String mBackGround;
    String mCharacter;
    String mMessage;
    String mDstPhoneNumber;
    ImageView cheracterImageView;
    ImageView backGroundImageView;
    GlobalVariables global;
    DatabaseReference mPagesDB;
    List<Map<String,Object>> pages_list;

    //
    boolean mIsPress = false;
    RelativeLayout BackroundChanging;
    RelativeLayout DarkerBackround;
    RelativeLayout SlideViewPager;
    private FeatureCoverFlow coverFlow;
    private BackroundSlideAdapter adapter;
    private ArrayList<Backround> Backround;
    private String figure;
    private ImageView BubbleMessage;
    private RelativeLayout LastChatMsg;
    //

    Button send_btn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_struct);
        global = new GlobalVariables();
        this.mBackGround = "grandpa_with_guitar";
        cheracterImageView = (ImageView) findViewById(R.id.CharecterImageView);
        backGroundImageView = (ImageView)findViewById(R.id.BackGroundViewImage);

        //
        BackroundChanging = (RelativeLayout) findViewById(R.id.backround_change_button);
        DarkerBackround = (RelativeLayout) findViewById(R.id.dark_screen);
        SlideViewPager = (RelativeLayout) findViewById(R.id.SlideViewPager);
        BubbleMessage = (ImageView) findViewById(R.id.MsgBubbleImageView);
        //LastChatMsg = (RelativeLayout) findViewById(R.id.last_chat_msg);
        //

        //
        coverFlow = (FeatureCoverFlow) findViewById(R.id.coverflow);
        settingDummyData();
        adapter = new BackroundSlideAdapter(this, Backround);
        coverFlow.setAdapter(adapter);
        coverFlow.setOnScrollPositionListener(onScrollListener());
        //

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("users").child(mAuth.getUid());
        mDatabase.addValueEventListener(chat_struct_listener);
        send_btn = (Button) findViewById(R.id.send_btn);
        send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText output = (EditText) findViewById(R.id.msg_output);
                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("public_queues").child(global.getDstUserID());
                ChatMessage chatMsg = new ChatMessage(mCharacter,mBackGround,output.getText().toString(),global.getDstUserID(),mAuth.getUid(),"temp", "temp", "temp");
                mDatabase.push().setValue(chatMsg);
                DatabaseReference myDatabase = FirebaseDatabase.getInstance().getReference().child("users").child(mAuth.getUid()).child("books").child(global.getDstUserID());
                myDatabase.push().setValue(chatMsg);
                startActivity(new Intent(ChatStruct.this, CarouselPreviewActivity.class));
                finish();
            }
        });


        //backGroundImageView.setOnLongClickListener(new View.OnLongClickListener() {
        //    @Override
        //    public boolean onLongClick(View v) {
        //        LastChatMsg.setVisibility(View.VISIBLE);
        //       LastChatMsg.setBackgroundColor(data.backroundImage.get(data.backroundImage.size() - 1));
        //       DarkerBackround.setAlpha(0.5f);
        //       DarkerBackround.setVisibility(View.VISIBLE);
        //       return false;
        //    }
        //});


        //Ron edit
        BackroundChanging.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mIsPress)
                {
                    DarkerBackround.animate().alpha(0).setDuration(300);
                    SlideViewPager.animate().alpha(0).setDuration(300);
                    mIsPress = !mIsPress;
                    DarkerBackround.setVisibility(View.GONE);
                }
                else
                {
                    DarkerBackround.setVisibility(View.VISIBLE);
                    DarkerBackround.animate().alpha(0.5f).setDuration(300);
                    SlideViewPager.animate().alpha(1).setDuration(300);
                    mIsPress = !mIsPress;
                }
            }
        });

        DarkerBackround.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mIsPress)
                {
                    DarkerBackround.animate().alpha(0).setDuration(200);
                    SlideViewPager.animate().alpha(0).setDuration(0);
                    mIsPress = !mIsPress;
                    DarkerBackround.setVisibility(View.GONE);
                }
            }
        });
        //
    }



    ValueEventListener chat_struct_listener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            User user = dataSnapshot.getValue(User.class);
            mCharacter = user.figure;
            //mBackGround = global.getScreen();
            switch (mCharacter){
                case "grandpa_with_guitar":
                    cheracterImageView.setImageResource(R.drawable.grandpa_with_guitar);
                    break;
                case "grandpa_on_chair":
                    cheracterImageView.setImageResource(R.drawable.grandpa_on_chair);
                    break;
                case "man_with_radio":
                    cheracterImageView.setImageResource(R.drawable.man_with_radio);

                    break;
                case "man_with_paresuit":
                    cheracterImageView.setImageResource(R.drawable.man_with_paresuit);

                    break;
                case "red_dress_women":
                    cheracterImageView.setImageResource(R.drawable.red_dress_women);

                    break;
            }

            switch (mBackGround){
                case "dot_back":
                    backGroundImageView.setImageResource(R.drawable.dot_back);
                    break;
                case "room_back":
                    backGroundImageView.setImageResource(R.drawable.room_back);
                    break;
                case "gray_back":
                    backGroundImageView.setImageResource(R.drawable.gray_back);
                    break;
                case "sea_backs_quare":
                    backGroundImageView.setImageResource(R.drawable.sea_backs_quare);
                    break;
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            //problema
        }
    };


    public Integer[] arrFactor(Integer[] arr){
        Integer[] newArr = new Integer[arr.length + 1];
        if(arr.length > 0){
            for (int i = 0; i < arr.length; i++)
            {
                newArr[i] = arr[i];
            }
        }
        newArr[newArr.length -1] = R.color.color_8;
        return newArr;
    }

    //RON EDIT
    private FeatureCoverFlow.OnScrollPositionListener onScrollListener() {
        return new FeatureCoverFlow.OnScrollPositionListener() {
            @Override
            public void onScrolledToPosition(int position) {
                Log.v("MainActiivty", "position: " + position);
                pickFigure(position);
            }

            @Override
            public void onScrolling() {
                Log.i("MainActivity", "scrolling");
            }
        };
    }

    private void pickFigure(int curPos) {
        switch(curPos){
            case 0:
                figure = "dot_back";
                backGroundImageView.setImageResource(R.drawable.dot_back);
                break;
            case 1:
                figure = "room_back";
                backGroundImageView.setImageResource(R.drawable.room_back);
                break;
            case 2:
                figure = "sea_back";
                backGroundImageView.setImageResource(R.drawable.sea_backs_quare);
                break;
            case 3:
                figure = "gray_back";
                backGroundImageView.setImageResource(R.drawable.gray_back);
                break;
            default:
                figure = "gray_back";
                break;
        }
    }

    private void settingDummyData() {
        Backround = new ArrayList<>();
        Backround.add(new Backround(R.drawable.dot_back_square));
        Backround.add(new Backround(R.drawable.room_back_square));
        Backround.add(new Backround(R.drawable.sea_backs_quare));
        Backround.add(new Backround(R.drawable.gray_back));
    }
    //

}