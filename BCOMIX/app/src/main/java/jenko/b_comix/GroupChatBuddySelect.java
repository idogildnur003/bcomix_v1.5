package jenko.b_comix;

import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.ContactsContract;
import android.renderscript.Sampler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GroupChatBuddySelect extends AppCompatActivity implements BubbleFragment.OnSelectClickListener {

    Button nextBtn;
    ImageView mAddBuddyImageView;
    Bundle b;
    FirebaseAuth mAuth;
    LinearLayout mBuddysView;
    List<Map<String,Object>> mAllFragments;
    DatabaseReference mFriendsDB;
    DatabaseReference mNicknamesDB;
    Map<String,Object> mNicknamesMap;
    Set<String> mSelectedUsers;
    GlobalVariables global;
    AddBuddyFragment tempFragment;
    private ProgressDialog loading;
    private Boolean loadingFlag;
    boolean side; //true -> left, false -> right


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_chat_buddy_select);
        mAuth = FirebaseAuth.getInstance();
        mNicknamesDB = FirebaseDatabase.getInstance().getReference().child("nicknames");
        mNicknamesDB.addValueEventListener(NicknamesListener);
        mFriendsDB = FirebaseDatabase.getInstance().getReference().child("users").child(mAuth.getUid()).child("friends");
        mFriendsDB.addValueEventListener(friendsListener);
        nextBtn = (Button)findViewById(R.id.next_btn);
        nextBtn.setOnClickListener(nextClickListener);
        mSelectedUsers = new HashSet<>();

        side = false;//start from right;
        mBuddysView = (LinearLayout)findViewById(R.id.buddys_linear_layout);

        mAllFragments = new ArrayList<>();

        loadingFlag = false;
        loading = new ProgressDialog(this);
        loading.show();
        loading.setMessage("Uploading Contacts...");
        loading.setCanceledOnTouchOutside(false);




    }


    ValueEventListener NicknamesListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if(dataSnapshot.exists()){
                mNicknamesMap = (Map<String, Object>) dataSnapshot.getValue();
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) { }
    };

    Button.OnClickListener nextClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(GroupChatBuddySelect.this,ActivityCreateBook.class);
            intent.putExtra("selected_users",mSelectedUsers.toArray());
            startActivity(intent);
        }
    };

    ImageView.OnClickListener addBuddysListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            FragmentManager fm = getFragmentManager();
            tempFragment = new AddBuddyFragment();
            Intent searchIntent = new Intent(GroupChatBuddySelect.this,AddBuddyFragment.class);
            startActivity(searchIntent);
        }
    };

    ValueEventListener friendsListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if(dataSnapshot.exists()){
                loadingFlag = true;
                Map<String,Object> friends_map = new HashMap<>();
                friends_map = (Map<String, Object>) dataSnapshot.getValue();
                if(friends_map!=null && !friends_map.isEmpty()){
                    for(final Object friendID: friends_map.values()){
                        DatabaseReference nicknamesDB = FirebaseDatabase.getInstance().getReference().child("nicknames");
                        nicknamesDB.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                Map<String, Object> temp_map = (Map<String, Object>) dataSnapshot.getValue();
                                String name = (String) mNicknamesMap.get(friendID);
                                BubbleFragment friendFragment = BubbleFragment.newInstance(name,side,(String)friendID);
                                Map<String,Object> fragmentData = new HashMap<>();
                                fragmentData.put("fragment",friendFragment);
                                fragmentData.put("name",name);
                                mAllFragments.add(fragmentData);
                                getSupportFragmentManager()
                                        .beginTransaction()
                                        .add(R.id.buddys_linear_layout,friendFragment,"BubbleBuddy")
                                        .commitAllowingStateLoss();
                                side = !side;

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                    loading.dismiss();
                }
            }
            else{
                loadingFlag = false;
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    @Override
    public void onSelectClickListener(String nickname, String userID) {
        nextBtn.setEnabled(true);
        for(Map<String,Object> fragment: mAllFragments){
            if(nickname.equals((String)fragment.get("name"))&& !mSelectedUsers.contains(nickname)){//if this is the selected nickname and its not already clicked
                mSelectedUsers.add(userID);
            }
            else{
                BubbleFragment tempFragment = (BubbleFragment)fragment.get("fragment");
                tempFragment.hidecheck();
                FragmentManager fm = getFragmentManager();
                mBuddysView.childDrawableStateChanged(tempFragment.getView());
                mSelectedUsers.remove(nickname);
            }
        }





    }
}
