package jenko.b_comix;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MainMenu extends AppCompatActivity {

    private ImageView options;
    private ImageView createNexComic;
    private ImageView inviteFriend;
    private ProgressDialog loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        loading = new ProgressDialog(this);
        options = findViewById(R.id.options_button);
        options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainMenu.this, Options.class));
                finish();
            }
        });

        createNexComic =  findViewById(R.id.start_new_comic);
        createNexComic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainMenu.this, BodySelect.class));
                finish();
            }
        });

        inviteFriend = findViewById(R.id.invite_friends);
        inviteFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loading.show();
                loading.setMessage("Uploading Contacts");
                loading.setCanceledOnTouchOutside(false);
                loading.show();
                startActivity(new Intent(MainMenu.this,InviteFriendActivity.class));
            }
        });


    }
}
