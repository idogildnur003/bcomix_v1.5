package jenko.b_comix;

import android.Manifest;
import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;

public class InviteFriendActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friend);
        /*ActivityCompat.requestPermissions(InviteFriendActivity.this,
                new String[]{Manifest.permission.READ_CONTACTS},
                1);*/
        int colorIndex = 0;
        ContentResolver contentResolver = getContentResolver();
        LinearLayout ContactLayout = (LinearLayout)findViewById(R.id.contactLinearLayout);
        Cursor cursor_Android_Contacts = null;

        //cursor_Android_Contacts.close();
        cursor_Android_Contacts = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if(cursor_Android_Contacts.getCount()>0){
            while(cursor_Android_Contacts.moveToNext()){
                String name = cursor_Android_Contacts.getString(cursor_Android_Contacts.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                String contact_id = cursor_Android_Contacts.getString(cursor_Android_Contacts.getColumnIndex(ContactsContract.Contacts._ID));
                int hasPhoneNumber = Integer.parseInt(cursor_Android_Contacts.getString(cursor_Android_Contacts.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
                String  phoneNumber = "";
                if(hasPhoneNumber>0) {
                    Cursor phoneCursor = contentResolver.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI
                            , null
                            , ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?"
                            , new String[]{contact_id}
                            , null);

                    while (phoneCursor.moveToNext()) {
                        phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    }
                    phoneCursor.close();
                }
                List<String> colors = new ArrayList<>();
                colors.add("red");
                colors.add("green");
                colors.add("yellow");
                colors.add("blue");
                ContactDetailsFragment contactDetailsFragment = ContactDetailsFragment.newInstance(name,phoneNumber,colors.get(colorIndex));
                if(!phoneNumber.equals("")){
                    getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.contactLinearLayout,contactDetailsFragment,"ContactFragment")
                            .commit();
                    colorIndex++;
                    if(colorIndex==4){
                        colorIndex = 0;
                    }
                }


            }
        }

    }
}
