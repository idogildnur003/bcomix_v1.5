package jenko.b_comix;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import com.azoft.carousellayoutmanager.CarouselLayoutManager;
import com.azoft.carousellayoutmanager.CarouselZoomPostLayoutListener;
import com.azoft.carousellayoutmanager.CenterScrollListener;
import com.azoft.carousellayoutmanager.DefaultChildSelectionListener;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import jenko.b_comix.databinding.ActivityCarouselPreviewBinding;
import jenko.b_comix.databinding.ItemViewBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

public class CarouselPreviewActivity extends AppCompatActivity {

    DatabaseReference mBookDB;
    FirebaseAuth mAuth;
    GlobalVariables global;
    Toolbar bar;
    List<ChatMessage> mBookData;
    Map<String,Object> names_map;
    String mChatType;
    String mDstID;
    String mName;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        mChatType = extras.getString("type");
        mDstID = extras.getString("dst");
        mName = extras.getString("name");




        final ActivityCarouselPreviewBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_carousel_preview);
        final TestAdapter adapter = new TestAdapter();
        initRecyclerView(binding.listVertical, new CarouselLayoutManager(CarouselLayoutManager.VERTICAL, false), adapter);
        DatabaseReference namesDB = FirebaseDatabase.getInstance().getReference().child("nicknames");
        namesDB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    names_map = (Map<String, Object>) dataSnapshot.getValue();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mBookData = new ArrayList<>();
        global = new GlobalVariables();
        mAuth = FirebaseAuth.getInstance();
        data.backroundImage.clear();
        data.characterImage.clear();
        data.texts.clear();
        data.titles.clear();
        mBookDB = FirebaseDatabase.getInstance().getReference().child("users").child(mAuth.getUid()).child("books").child(global.getDstUserID());
        mBookDB.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                if(dataSnapshot.exists()) {
                    ChatMessage temp = dataSnapshot.getValue(ChatMessage.class);
                    mBookData.add(temp);
                    switch (temp.mBackground) {
                        case "dot_back":
                            data.backroundImage.add(R.drawable.dot_back);
                            break;
                        case "room_back":
                            data.backroundImage.add(R.drawable.room_back);
                            break;
                        case "gray_back":
                            data.backroundImage.add(R.drawable.gray_back);
                            break;
                        case "sea_backs_quare":
                            data.backroundImage.add(R.drawable.sea_backs_quare);
                            break;
                    }
                    switch (temp.mFigure) {
                        case "grandpa_with_guitar":
                            data.characterImage.add(R.drawable.grandpa_with_guitar);
                            break;
                        case "grandpa_on_chair":
                            data.characterImage.add(R.drawable.grandpa_on_chair);
                            break;
                        case "man_with_radio":
                            data.characterImage.add(R.drawable.man_with_radio);
                            break;
                        case "man_with_paresuit":
                            data.characterImage.add(R.drawable.man_with_paresuit);
                            break;
                        case "red_dress_women":
                            data.characterImage.add(R.drawable.red_dress_women);
                            break;
                    }
                    data.texts.add(temp.mText);
                    data.titles.add((String) names_map.get(temp.mSrcUserID));
                    adapter.notifyItemInserted(data.backroundImage.size());
                    binding.listVertical.smoothScrollToPosition(data.backroundImage.size() - 1);

                    bar = (Toolbar) findViewById(R.id.toolbar6);
                    setSupportActionBar(bar);
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    getSupportActionBar().setTitle(global.getName());
                    getSupportActionBar().setSubtitle("Connected");
                    bar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //setSupportActionBar(binding.toolbar);

        // create layout manager with needed params: vertical, cycle


        // fab button will add element to the end of the list
        binding.fabScroll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                //HERE YOU ARE INSERNT THE NEW DATA (THAT YOU SEND OR SOMEONE SEND TO YOU)
                Intent chatStructIntent = new Intent(CarouselPreviewActivity.this,ChatStruct.class);
                startActivity(chatStructIntent);
                finish();
            }
        });

        // fab button will remove element from the end of the list

    }

    private void initRecyclerView(final RecyclerView recyclerView, final CarouselLayoutManager layoutManager, final TestAdapter adapter) {
        // enable zoom effect. this line can be customized
        layoutManager.setPostLayoutListener(new CarouselZoomPostLayoutListener());
        layoutManager.setMaxVisibleItems(2);

        recyclerView.setLayoutManager(layoutManager);
        // we expect only fixed sized item for now
        recyclerView.setHasFixedSize(true);
        // sample adapter with random data
        recyclerView.setAdapter(adapter);
        // enable center post scrolling
        recyclerView.addOnScrollListener(new CenterScrollListener());
        // enable center post touching on item and item click listener
        DefaultChildSelectionListener.initCenterItemListener(new DefaultChildSelectionListener.OnCenterItemClickListener() {
            @Override
            public void onCenterItemClicked(@NonNull final RecyclerView recyclerView, @NonNull final CarouselLayoutManager carouselLayoutManager, @NonNull final View v) {
                final int position = recyclerView.getChildLayoutPosition(v);
                final String msg = String.format(Locale.US, "Item %1$d was clicked", position);
                Toast.makeText(CarouselPreviewActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        }, recyclerView, layoutManager);

        layoutManager.addOnItemSelectionListener(new CarouselLayoutManager.OnCenterItemSelectionListener() {

            @Override
            public void onCenterItemChanged(final int adapterPosition) {
                if (CarouselLayoutManager.INVALID_POSITION != adapterPosition) {
                    //final int value = adapter.mPosition[adapterPosition];

                    //adapter.mPosition[adapterPosition] = (value % 10) + (value / 10 + 1) * 10;
                    //adapter.notifyItemChanged(adapterPosition);
                }
            }
        });
    }

    //CREATE ALL THE CARDS IN THE LOOP
    private static final class TestAdapter extends RecyclerView.Adapter<TestViewHolder> {

        @SuppressWarnings("UnsecureRandomNumberGeneration")
        private int mItemsCount = 0;


        @Override
        public TestViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
            Log.e("!!!!!!!!!", "onCreateViewHolder");
            return new TestViewHolder(ItemViewBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
        }

        @Override
        public void onBindViewHolder(final TestViewHolder holder, final int position) {
            Log.e("!!!!!!!!!", "onBindViewHolder: " + position);

            //HERE YOU ARE ADD THE DATA BY THE POS
            //THE "backroundImage" IS JUST FOR EXAMPLE, YOU CAN CHANGE THE BODY, TITLE, TEXT ETC. (BY THE XML PARAMETERS)
            holder.mItemViewBinding.textTextView.setText(data.texts.get(position));
            holder.mItemViewBinding.materialrecentsRecentContent.setBackgroundResource(data.backroundImage.get(position));
            holder.mItemViewBinding.CharecterImageView.setImageResource(data.characterImage.get(position));
            holder.mItemViewBinding.materialrecentsRecentTitle.setText(data.titles.get(position));
            holder.itemView.setBackgroundColor(Color.argb(255,255, 133, 51));
        }

        @Override
        public int getItemCount() {
            return data.backroundImage.size();
        }

        @Override
        public long getItemId(final int position) {
            return position;
        }
    }

    private static class TestViewHolder extends RecyclerView.ViewHolder {

        private final ItemViewBinding mItemViewBinding;

        TestViewHolder(final ItemViewBinding itemViewBinding) {
            super(itemViewBinding.getRoot());

            mItemViewBinding = itemViewBinding;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.chat_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){

            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(new Intent(CarouselPreviewActivity.this, LaunchActivity.class))
                            .startActivities();
                } else {
                    NavUtils.navigateUpTo(this, new Intent(CarouselPreviewActivity.this, LaunchActivity.class));
                }
                finish();
                return true;

            case R.id.settings:
                startActivity(new Intent(CarouselPreviewActivity.this, Options.class));
                break;

            case R.id.edit_prof:
                startActivity(new Intent(CarouselPreviewActivity.this, EditProfile.class));
                break;

            case R.id.sign_out:
                FirebaseAuth.getInstance().signOut();
                LoginManager.getInstance().logOut();
                Toast.makeText(CarouselPreviewActivity.this, "Signed Out Successfuly", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(CarouselPreviewActivity.this, MainActivity.class));
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
