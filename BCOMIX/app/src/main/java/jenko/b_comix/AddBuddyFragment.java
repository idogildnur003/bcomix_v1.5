package jenko.b_comix;

import android.app.Activity;
import android.content.Context;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AddBuddyFragment extends AppCompatActivity {

   DatabaseReference mDataBase;
   DatabaseReference mUserDB;
   FirebaseAuth mAuth;
   Map<String,Object> mAllNicknames;
   Map<String,Object> mMyFriends;

   EditText mSearchNameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_add_buddy);
        mAuth = FirebaseAuth.getInstance();
        mDataBase = FirebaseDatabase.getInstance().getReference().child("nicknames");
        mUserDB = FirebaseDatabase.getInstance().getReference().child("users").child(mAuth.getUid());
        mUserDB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Map<String,Object> tempMap = (HashMap<String,Object>)dataSnapshot.getValue();
                if(tempMap.keySet().contains("friends")) {
                    mMyFriends = (Map<String, Object>) tempMap.get("friends");
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

        mDataBase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mAllNicknames = (Map<String, Object>) dataSnapshot.getValue();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        updateUsersScrollView();

        mAllNicknames = new HashMap<>();
        mSearchNameEditText = findViewById(R.id.nameSearchEditText);

        mSearchNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                updateUsersScrollView();
            }
        });
    }

    public void updateUsersScrollView(){
        LinearLayout lay = findViewById(R.id.searchLinearLayout);
        lay.removeAllViews();
        List<String> colors = Arrays.asList("red", "green", "blue", "yellow");
        int index = 0;
        if(mAllNicknames!=null && !mAllNicknames.isEmpty()) {
            for (String m : mAllNicknames.keySet()) {
                if (mAllNicknames.get(m).toString().equals(mSearchNameEditText.getText().toString()) || mAllNicknames.get(m).toString().contains(mSearchNameEditText.getText().toString())) {
                    if (!mAllNicknames.get(m).toString().equals("")) {
                        ContactDetailsFragment temp = ContactDetailsFragment.newInstance(mAllNicknames.get(m).toString(), "search&&" + m, colors.get(index));
                        getSupportFragmentManager()
                                .beginTransaction()
                                .add(R.id.searchLinearLayout, temp, "myTag")
                                .commit();
                        index++;
                        if (index == 4) {
                            index = 0;
                        }
                    }

                }
            }
        }
    }


}
