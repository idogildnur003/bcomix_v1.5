package jenko.b_comix;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

public class BackroundSlideAdapter extends BaseAdapter {

    private ArrayList<Backround> data;
    private AppCompatActivity activity;

    public BackroundSlideAdapter(AppCompatActivity context, ArrayList<Backround> objects) {
        this.activity = context;
        this.data = objects;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Backround getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        BackroundSlideAdapter.ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_flow_view, null, false);

            viewHolder = new BackroundSlideAdapter.ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (BackroundSlideAdapter.ViewHolder) convertView.getTag();
        }

        viewHolder.BackroundImage.setImageResource(data.get(position).getImageSource());

        return convertView;
    }


    private static class ViewHolder {
        private ImageView BackroundImage;

        public ViewHolder(View v) {
            BackroundImage = (ImageView) v.findViewById(R.id.image);
        }
    }
}