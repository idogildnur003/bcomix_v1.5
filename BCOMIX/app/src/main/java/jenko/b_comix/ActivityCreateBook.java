package jenko.b_comix;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class ActivityCreateBook extends AppCompatActivity {

    Button mNextBtn;
    EditText mBooknameEdittext;
    String mSelectedBook;
    String[] mSelectedUsers;
    FirebaseAuth mAuth;
    GlobalVariables global;

    ImageView mSeaImageView;
    ImageView mRoomImageView;
    ImageView mGrayImageView;
    ImageView mDotsImageView;
    ImageView mIceCreamImageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_book);
        global = new GlobalVariables();
        mAuth = FirebaseAuth.getInstance();
        mBooknameEdittext = findViewById(R.id.bookNameEditText);
        mNextBtn = findViewById(R.id.next_btn);
        mSeaImageView = findViewById(R.id.sea_back_mockup);
        mRoomImageView = findViewById(R.id.room_back_mockup);
        mGrayImageView = findViewById(R.id.gray_back_mockup);
        mDotsImageView = findViewById(R.id.dots_back_mockup);
        mIceCreamImageView = findViewById(R.id.icecream_back_mockup);

        Bundle extras =getIntent().getExtras();
        mSelectedUsers = extras.getStringArray("selected_users");

        mSeaImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNextBtn.setEnabled(true);
                mSelectedBook = "sea";
            }
        });
        mRoomImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNextBtn.setEnabled(true);
                mSelectedBook = "room";
            }
        });
        mGrayImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNextBtn.setEnabled(true);
                mSelectedBook = "gray";
            }
        });
        mDotsImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNextBtn.setEnabled(true);
                mSelectedBook = "dots";
            }
        });
        mIceCreamImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNextBtn.setEnabled(true);
                mSelectedBook = "icecream";
            }
        });

        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String key = FirebaseDatabase.getInstance().getReference().child("groups_queues").push().getKey();
                Map<String,Object> temp_map = new HashMap<>();
                temp_map.put("name",mBooknameEdittext.getText().toString());
                temp_map.put("design",mSelectedBook);
                FirebaseDatabase.getInstance().getReference().child("groups_queues").child(key).child("data").setValue(temp_map);
                for(String userID : mSelectedUsers){
                    FirebaseDatabase.getInstance().getReference().child("friends_request").child(userID).setValue("group_invite&&"+mAuth.getUid()+"&&"+key+"$$"+global.getMyNickname()+"$$"+mBooknameEdittext.getText().toString());
                }
                FirebaseDatabase.getInstance().getReference().child("users").child(mAuth.getUid()).child("groups").push().setValue(key);
                Intent intent = new Intent(ActivityCreateBook.this,CarouselPreviewActivity.class);
                intent.putExtra("type","group");
                intent.putExtra("dst",key);
                intent.putExtra("name",mBooknameEdittext.getText().toString());
                startActivity(intent);
            }
        });




    }
}
